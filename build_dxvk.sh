#!/bin/sh

git clone https://github.com/Joshua-Ashton/dxvk-native
TOP=`pwd`

cd dxvk-native
meson --buildtype "release" --prefix /usr/local/ -Dbuild_id=false build
cd build
ninja install

LIB_DIR=$(meson introspect --installed | python3 -c 'import sys,json; o = list(json.loads(sys.stdin.read()).values()); print("/".join(o[0].split("/")[:-1]))')
#install headers
cd $TOP;
INC_DIR=/usr/local/include/dxvk-native
mkdir -p ${INC_DIR}
cp -rv dxvk-native/include/* ${INC_DIR}/

rm -rf dxvk-native
sed -i -e "/includedir=/ s%.+%includedir=${INC_DIR}%" -e "/libdir=/ s%.+%libdir=${LIB_DIR}%" /usr/share/pkgconfig/dxvk-native.pc
